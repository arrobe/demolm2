<?php

namespace App\Form;

use App\Entity\Fournisseur;
use App\Entity\Civilite;
use App\Entity\Ville;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FournisseurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('contact')
            ->add('adresse')
            ->add('civilite', EntityType::class, [
              'class' => Civilite::class,
              'choice_label' => 'libelle'
            ])
            ->add('ville', EntityType::class, [
              'class' => Ville::class,
              'choice_label' => 'nom'
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Fournisseur::class,
        ]);
    }
}
